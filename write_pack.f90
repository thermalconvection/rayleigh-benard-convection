module write_pack

use global

contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine write_vec(vname,place, vec)

character(len=*), intent(in) :: vname,place
real(dp), intent(in) :: vec(:)
integer :: ii, n

n = size(vec)

do ii = 1,n
   if (ii == 1) then
      write(*,*) vname//" = ", vec(ii)
   else
      write(*,*) place//"   ", vec(ii)
   end if
end do

end subroutine write_vec

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine write_mat(vname,place, mat)

character(len=*), intent(in) :: vname,place
real(dp), intent(in)         :: mat(:,:)
integer                      :: jj, mx,my

mx = size(mat(1,:))
my = size(mat(:,1))

do jj = 1,my
   if (jj==1) then
      write(*,*) vname//" = ", mat(jj,:)
   else
      write(*,*) place//"   ", mat(jj,:)
   end if
end do

end subroutine write_mat

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine write_vec_cmplx(vname,place,vec)

character(len=*), intent(in) :: vname, place
complex(dp), intent(in) :: vec(:)
integer :: ii, n

n = size(vec)

do ii = 1,n
   if (ii == 1) then
      write(*,*) vname//" = ", vec(ii)
   else
      write(*,*) place//"   ", vec(ii)
   end if
end do

end subroutine write_vec_cmplx

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine write_mat_cmplx(vname,place, mat)

character(len=*), intent(in) :: vname,place
complex(dp), intent(in) :: mat(:,:)
integer :: jj, mx,my

mx = size(mat(1,:))
my = size(mat(:,1))

do jj = 1,my
   if (jj==1) then
      write(*,*) vname//" = ", mat(jj,:)
   else
      write(*,*) place//"   ", mat(jj,:)
   end if
end do

end subroutine write_mat_cmplx

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine write_vtk_structured_grid(iter)

use global

implicit none

integer, intent(in) :: iter
character(10) :: citer
character(9)  :: fiter
integer :: n
integer :: ii, jj, kk

write(citer, "(I10)") 1000000000 + iter
fiter = citer(2:10)

n = (Nx+1)*Ny*Nz

!write(*,*) "filename= ", vtkloc//vtkname//fiter//gtype

open(unit=2, file=vtkloc//vtkname//fiter//gtype, action="write", status="replace")
write(2, "(a)") "# vtk DataFile Version 3.1"
write(2, "(a)") "Thermal Convection Data"
write(2, "(a)") "ASCII"
write(2, "(a)") "DATASET STRUCTURED_GRID"
write(2, "(a, 3I5.1)"), "DIMENSIONS", Nx+1, Ny, Nz
write(2, "(a, I15.1, a6)"), "POINTS", n, "float"

do kk = 1,Nz
   do jj = 1,Ny
      do ii = 1,Nx+1
         if (ii < Nx+1) then
            write(2,"(3F18.14)") xp(ii), yp(jj), zp(kk)
         else
            write(2,"(3F18.14)") xR, yp(jj), zp(kk)
         end if
      end do
   end do
end do

write(2,"(a)") " "
write(2,"(a, a, I15.1)"), "POINT_DATA", " ", n 
write(2,"(a, a, a, a, a)") "SCALARS", " ", "Temperature", " ", "float"
write(2,"(a, a, a)") "LOOKUP_TABLE", " ", "default"

do kk = 1,Nz
   do jj = 1,Ny
      do ii = 1,Nx+1
         if (ii < Nx+1) then
            write(2, "(F17.14)") real(T(jj,ii))
         else
            write(2, "(F17.14)") real(T(jj,1))
         end if
      end do
   end do
end do

write(2,"(a)") " "
write(2,"(a, a, a, a, a)") "SCALARS", " ", "ux", " ", "float"
write(2,"(a, a, a)") "LOOKUP_TABLE", " ", "default"

do kk = 1,Nz
   do jj = 1,Ny
      do ii = 1,Nx+1
         if (ii < Nx+1) then
            write(2, "(F17.14)") real(ux(jj,ii))
         else
            write(2, "(F17.14)") real(ux(jj,1))
         end if
      end do
   end do
end do

write(2,"(a)") " "
write(2,"(a, a, a, a, a)") "SCALARS", " ", "uy", " ", "float"
write(2,"(a, a, a)") "LOOKUP_TABLE", " ", "default"

do kk = 1,Nz
   do jj = 1,Ny
      do ii = 1,Nx+1
         if (ii < Nx+1) then
            write(2, "(F17.14)") real(uy(jj,ii))
         else
            write(2, "(F17.14)") real(uy(jj,1))
         end if
      end do
   end do
end do


close(unit=2)

end subroutine write_vtk_structured_grid

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine write_to_vtk(step, fft)

integer, intent(in) :: step, fft
integer             :: i, j

select case (fft)

   case (0)

      call write_vtk_structured_grid(step)

   case (1)

      do j = 1,Ny
         ! Bring everything to physical space
         tT  = T(j,:)
         tuy = uy(j,:)
         tux = ux(j,:)
         call fftw_execute_dft(iplanux, tux, tux)
         call fftw_execute_dft(iplanuy, tuy, tuy)
         call fftw_execute_dft(iplanT, tT, tT)
         T(j,:)   = tT
         ux(j,:)  = tux
         uy(j,:)  = tuy
      end do
      call write_vtk_structured_grid(step)
      do j = 1,Ny
         ! Bring everything to Fourier space
         tT  = T(j,:)
         tuy = uy(j,:)
         tux = ux(j,:)
         call fftw_execute_dft(planux, tux, tux)
         call fftw_execute_dft(planuy, tuy, tuy)
         call fftw_execute_dft(planT, tT, tT)
         ! Dealias
         do i = 1,Nx
            if (abs(kx(i))/alpha >= Nf/2) then
               tT(i)  = cmplx(0.0_dp, 0.0_dp, kind=C_DOUBLE_COMPLEX)
               tux(i) = cmplx(0.0_dp, 0.0_dp, kind=C_DOUBLE_COMPLEX)
               tuy(i) = cmplx(0.0_dp, 0.0_dp, kind=C_DOUBLE_COMPLEX)
            end if
         end do
         T(j,:)   = tT
         ux(j,:)  = tux
         uy(j,:)  = tuy
      end do
      T  = T  / real(Nx, kind=dp)
      ux = ux / real(Nx, kind=dp)
      uy = uy / real(Nx, kind=dp)

end select

end subroutine write_to_vtk
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine write_restart

integer :: i, j

do j = 1,Ny
   ! Bring everything to physical space
   tT  = T(j,:)
   tuy = uy(j,:)
   call fftw_execute_dft(iplanuy, tuy, tuy)
   call fftw_execute_dft(iplanT, tT, tT)
   T(j,:)   = tT
   uy(j,:)  = tuy
end do

open(unit=2,file="uy_new",action="write", status="replace", form="unformatted")
do ii = 1,Nx
   do jj = 1,Ny
      write(2) real(uy(jj,ii), kind=dp)
   end do
end do
close(unit=2)

open(unit=3,file="temperature_new",action="write", status="replace", form="unformatted")
do ii = 1,Nx
   do jj = 1,Ny
      write(3) real(T(jj,ii), kind=dp)
   end do
end do
close(unit=3)

do j = 1,Ny
   ! Bring everything to Fourier space
   tT  = T(j,:)
   tuy = uy(j,:)
   call fftw_execute_dft(planuy, tuy, tuy)
   call fftw_execute_dft(planT, tT, tT)
   ! Dealias
   do i = 1,Nx
      if (abs(kx(i))/alpha >= Nf/2) then
         tT(i)  = cmplx(0.0_dp, 0.0_dp, kind=C_DOUBLE_COMPLEX)
         tuy(i) = cmplx(0.0_dp, 0.0_dp, kind=C_DOUBLE_COMPLEX)
      end if
   end do
   T(j,:)   = tT
   uy(j,:)  = tuy
end do
T  = T  / real(Nx, kind=dp)
uy = uy / real(Nx, kind=dp)

end subroutine write_restart

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine write_to_txt(field)
! Accepts field as a vector.  This will work if you want to write
! out the velocity (for example) along a line.

complex(C_DOUBLE_COMPLEX), dimension(:), intent(in) :: field
integer :: jj

open(unit=8000, file="field-line.txt", action="write", status="unknown", position="append")
do jj = 1,Ny
   write(8000, fmt=2000) yp(jj), real(field(jj))
end do

close(unit=8000)

2000 format(E25.16E3,E25.16E3)

end subroutine write_to_txt

end module write_pack
